<#import "/spring.ftl" as spring>

<html>
    <head>
    <link rel="stylesheet" href="css/styles.css" type="text/css"/>
    </head>
    <body>
        <h1>Welcome to Tech Brothers - Tech Solutions</h1>
        <h3>User Form </h3>     
        <form action="user" method="POST">
            <div class="user-form">
                <div class="labels">
                    <label>User Name:</label>
                    <@spring.bind "user.name"/>
                    <@spring.formInput "user.name" "" "text"/>
                    <@spring.showErrors "<br>"/>
                </div>
                  <div class="labels">
                    <label>Age:</label>
                    <@spring.bind "user.age"/>
                    <@spring.formInput "user.age" "" "text"/>
                    <@spring.showErrors "<br>"/>
                </div>
                <div class="labels">
                    <label>Email-Id:</label>
                    <@spring.bind "user.emailId"/>
                    <@spring.formInput "user.emailId" "" "text"/>
                    <@spring.showErrors "<br>"/>
                </div>                                           
                <div>
                    <button type="submit" name="create">Create</button>
                </div>
            </div>
        </form> 
    </body>
</html>

