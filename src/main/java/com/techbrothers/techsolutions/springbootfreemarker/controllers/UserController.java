/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techbrothers.techsolutions.springbootfreemarker.controllers;

import com.techbrothers.techsolutions.springbootfreemarker.domain.User;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author chiru
 */
@Controller
@SessionAttributes({"user"})
public class UserController {
    
    @ModelAttribute("user")
    public User user(){
        return new User();
    }
    
    @GetMapping("/user")
    public String showUserInfo(Model model){
        model.addAttribute("user", new User());
        return "user";
    } 
    
    @PostMapping(value = "/user")
    public String createUser(@Valid User user, BindingResult bindingResult, 
            RedirectAttributes redirectAttributes){
        System.out.println("User inform" + user);
        redirectAttributes.addFlashAttribute("user", user);
        return "redirect:/display";
    }
}
